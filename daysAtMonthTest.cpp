/*
 * daysAtMonthTest.cpp
 *
 *  Created on: 22.04.2017
 *      Author: Magda
 */


#include "gtest/gtest.h"
#include "functions.h"


TEST (Days, Przestepny){
	EXPECT_EQ(31,daysAtMonth(3,2017));
}


TEST (Days, Nieprzestepny){
	EXPECT_EQ(30,daysAtMonth(11,2016));
}

TEST (Days, incorrectMonth){
	EXPECT_EQ(0,daysAtMonth(13,2016));
}

TEST (Days, incorrectYear){
	EXPECT_EQ(0,daysAtMonth(13,-1));
}
