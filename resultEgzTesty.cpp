/*
 * resultEgzTesty.cpp
 *
 *  Created on: 22.04.2017
 *      Author: Magda
 */


#include "gtest/gtest.h"
#include "functions.h"

TEST (EGZAM, Thirty){
	EXPECT_EQ(30,egzResult(30));
}


TEST (EGZAM, Forty){
	EXPECT_EQ(40,egzResult(40));
}


TEST (EGZAM, FortyOne){
	EXPECT_EQ(41,egzResult(41));
}


TEST (EGZAM, FortySeven){
	EXPECT_EQ(47,egzResult(47));
}


TEST (EGZAM, Negative){
	EXPECT_EQ(0,egzResult(-20));
}

TEST (EGZAM, Positive){
	EXPECT_EQ(50,egzResult(48));
}

TEST (EGZAM, Positive2){
	EXPECT_EQ(55,egzResult(55));
}

TEST (EGZAM, Positive3){
	EXPECT_EQ(71,egzResult(71));
}

TEST (EGZAM, Positive4){
	EXPECT_EQ(100,egzResult(99));
}



