#include "gtest/gtest.h"
#include "functions.h"

TEST (DigitNumberTest, NegativeHigh) {
	EXPECT_EQ(3,numberOfDigit(-222));
}

TEST (DigitNumberTest, NegativeTwo) {
	EXPECT_EQ(1,numberOfDigit(-2));
}

TEST (DigitNumberTest, Zero) {
	EXPECT_EQ(1,numberOfDigit(0));
}

TEST (DigitNumberTest, Five) {
	EXPECT_EQ(1,numberOfDigit(5));
}

TEST (DigitNumberTest, Ten) {
	EXPECT_EQ(2,numberOfDigit(10));
}

TEST (DigitNumberTest, PositiveHigh) {
	EXPECT_EQ(4,numberOfDigit(2432));
}
