#ifndef FUNCTIONS_H_
#define FUNCTIONS_H_
#include <iostream>
using namespace std;

extern int test;
//methods
int myPower(int number,int power);
int factorial(int number);
int fibonacciNumber(int result);
int greatestCommonDivisor(int firstNbr,int secondNbr );
int daysAtMonth(int month,int year);
int egzResult(int number);
int ratioDigitOfNumber(int number);
int numberOfDigit(int number);
string verbalDigitOfTheNumber(int number);

//bool isDataCorrect(int day,int month,int year);




#endif /* FUNCTIONS_H_ */
