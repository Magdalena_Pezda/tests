/*
 * functions.cpp
 *
 *  Created on: 21.04.2017
 *      Author: Magda
 */

#ifndef FUNCTIONS_CPP_
#define FUNCTIONS_CPP_

#include "functions.h"
#include <cmath>


int myPower(int number, int power){ //podnoszenie do dowolnej potegi
	return pow(number,power);
}

int factorial(int number) { //silnia
	if (number==0){
		return 1;
	}
	else if(number>=1){
		return number*factorial(number-1);
		}
	else if(number<0){
		return 0;
	}
return 0;
}

int fibonacciNumber(int n){ //fibonacci
	if (n==0 || n==1) {
		return 0;
	}
	else {
		int m;
		int f=0;
		int f1=1;
		for(int i=2; i<=n; i++){
			do{
				m=f+f1;
				f1=f;
				f=m;
			}
			while (i<n);
		return f;

		}
	}
	return 0;
}

int greatestCommonDivisor(int firstNbr, int secondNbr ){
	int n;
	while (secondNbr!=0) {
		n=firstNbr%secondNbr;
		firstNbr=secondNbr;
		secondNbr=n;
	}
	return abs(firstNbr);
}

int daysAtMonth(int month,int year){
	if (month==1 || month==3 || month==5 || month==7 ||month==8 ||month==10 || month==12) {
		return 31;
	}
	else if (month==4 || month==6 || month==9 || month==11) {
		return 30;
	}
	else if (month==2){
		if ((year%4==0 && year%100!=0) || year%400==0) {
			return 29;
		}
		else
			return 28;
	}
	return 0;
}

int egzResult(int number){
	if (number>=0 && number<=100) {
		if (number<=40){
			return number;
		}
		else if (number%5<3){
			return number;
		}
		else {
			return number=number+(5-number%5);
		}
	}
	else
		return 0;
}

int ratioDigitOfNumber(int number){
	int ratio=1;
	if (number==0){
		return 0;
	}
	while(number%10!=0){
		ratio*=number%10;
		number=number/10;
	}
	return abs(ratio);
}

int numberOfDigit(int number){
	int nbr=0;
	number=abs(number);
	do {
		number=number/10;
		nbr++;
	}
	while (number!=0);
	return nbr;
}









//
//string verbalDigitOfTheNumber(int number){
//	int digit;
//	int nbrOfDigits=0;
//	do {
//		number=number/10;
//		nbrOfDigits++;
//	}
//	while (number/10!=0);
//
//	string tab[nbrOfDigits];
//	while(nbrOfDigits!=0){
//		digit=number%10;
//		switch(digit){
//			case 0:
//				tab[nbrOfDigits]="zero";
//				break;
//			case 1:
//				break;
//			case 2:
//				break;
//			case 3:
//				break;
//			case 4:
//				break;
//			case 5:
//				break;
//			case 6:
//				break;
//			case 7:
//				break;
//			case 8:
//				break;
//			case 9:
//				break;
//		nbrOfDigits--;
//		}
//		number=number/10;
//	}
//
//}
////
//bool isDataCorrect(int day, int month, int year) {
//	bool valid=true;
//	 if (year>=0 && year <=2017) {
//		 if (month >=1 && month <=12) {
//			 if (day >=1) {
//				 if (month==1 || month==3 || month==5 || month==7 || month==8 || month==10 || month==12) {
//	                    if (day > 31){
//	                        valid=false;
//	                    }
//	                }
//	                else if (month==4 || month==6 || month==9 || month==11) {
//	                    if (day > 30) {
//
//	                        valid=false;
//	                    }
//	                }
//	                else {
//	                    if (year%4==0 && year%100!=0 || year%400==0) {
//	                        valid = false;
//	                    }
//	                    else if (day>28) {
//	                    	valid = false;
//	                    }
//	                }
//	            }
//	            else {
//	            	//"Podaj poprawny dzien!";
//	            	return false;
//	            }
//	            if(!valid)
//	            	return false;
//	        }
//	        else {
//	        	return=false;
//	        }
//	    }
//	    else {
//	    	return=false;
//	    }
//	return valid;
//}




#endif /* FUNCTIONS_CPP_ */
