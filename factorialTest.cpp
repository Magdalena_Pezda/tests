/*
 * factorialTest.cpp
 *
 *  Created on: 21.04.2017
 *      Author: Magda
 */

#include "gtest/gtest.h"
#include "functions.h"


TEST (factorial, Negative) {
	EXPECT_EQ(0,factorial(-2));
}

TEST (factorial, Zero) {
	EXPECT_EQ(1,factorial(0));
}

TEST (factorial, Positive){
	EXPECT_EQ(2,factorial(2));
}
