#include "gtest/gtest.h"
#include "functions.h"


TEST (RatioTest, Two) {
	EXPECT_EQ(2,ratioDigitOfNumber(2));
}

TEST (RatioTest, Zero ) {
	EXPECT_EQ(0,ratioDigitOfNumber(0));
}
TEST (RatioTest, Four) {
	EXPECT_EQ(4,ratioDigitOfNumber(4));
}

TEST (RatioTest, TwentyTwo ) {
	EXPECT_EQ(4,ratioDigitOfNumber(22));
}

TEST (RatioTest, Negative ) {
	EXPECT_EQ(8,ratioDigitOfNumber(-222));
}


TEST (RatioTest, Positive ) {
	EXPECT_EQ(8,ratioDigitOfNumber(222));
}
